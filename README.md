# ExchangeTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Comments

I didn't want to overextend in time, some there are some things left on the tasklist:

-  I tried to migrate to eslint, but had some troubles configuring it, so it runs with some rules it shouldn't
-  I would've liked to add some e2e tests, prior migriting it to Cyppress
-  Unit Testing: I didn't have much methods on the components, so there wasn't much to test. I left the prebuilt ones. One thing that could been tested is Snapshots, for the layout components like the app-card, but for that, first I would've had to migrate to Jest.
-  I've never used Tailwaind CSS, so to implement it, I would have to study it. I made a quick look into it, is not hard and looks simple, but, to be able to concentrate on the test, I decided not to implement it.

## Improvements

-  Clean styling and migrate to global sheets when possible (given a proper design book)
-  Add some animations when changing screen, adjusting layout to media, etc.
-  Improve eslint configuration
-  Migrate e2e tool to Cyppress
-  Migrate unit test tool to Jest
-  Consider migrating playlist-store to a higher level, before consuming it from other places in the app. There was left an "AddToFav" preimplementation, which adds playlist to Favs, so this playlist could've be shown in the Home screen. There is some logic to show the correct UX indicators on Playlist-collection to indicate it is already faved that should be correctly implmented. To see if it was already fav, I thought about doing it through a Pipe to see if is included in the idArray. Then, to Fave it, i thought about doing it through LocalStorage, so (if working with users), we could retreive it without depending on backend services (unless UX requires faves to not depend on device). 

## Thoughts
I've really enjoyed this test, it's been a while since I worked with a Redux implementation in Angular, I had mostly done it in React Apps. It felt nice to refresh it.
I hope it's up to what you are looking for, if there are extra things that you would like to see, please, feel free to do it.