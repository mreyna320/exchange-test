import { createFeatureSelector, createSelector } from '@ngrx/store';
import { playlistsFeatureKey, PlaylistsState } from './playlists.reducer';

export const getPaymentMethodState =
	createFeatureSelector<PlaylistsState>(playlistsFeatureKey);

export const getPlaylistsCollection = createSelector(
	getPaymentMethodState,
	(state: PlaylistsState) => state.playlists
);

export const getFavedPlaylists = createSelector(
	getPaymentMethodState,
	(state: PlaylistsState) =>
		state.playlists.filter((pl) => state.favs.includes(pl.id))
);

export const getFavedPlaylistsIds = createSelector(
	getPaymentMethodState,
	(state: PlaylistsState) => state.favs
);
