import { createAction, props } from '@ngrx/store';
import { IPlaylist } from './playlist.model';

export const loadPlaylists = createAction('[Playlists] Load Playlists');

export const loadPlaylistsSuccess = createAction(
	'[Playlists] Load Playlists Success',
	props<{ playlistsCollection: IPlaylist[] }>()
);

export const loadPlaylistsError = createAction(
	'[Playlists] Load Playlists Error'
);

export const addPlaylistToFav = createAction(
	'[PaymentMethod] Add Playlist to Favs',
	props<{ playlistToAdd: string }>()
);
