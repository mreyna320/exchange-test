export interface IPlaylistsResponse {
	featuredPlaylists: {
		name: string;
		content: IPlaylist[];
	};
}

export interface IPlaylist {
	id: string;
	kind: string;
	name: string;
	url: string;
	curator_name: string;
	artwork: string;
}
