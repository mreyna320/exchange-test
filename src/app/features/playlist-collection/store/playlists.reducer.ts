import { Action, createReducer, on } from '@ngrx/store';

import * as PlaylistsActions from './playlist.actions';
import { IPlaylist } from './playlist.model';

export const playlistsFeatureKey = 'playlists-collection';

export interface PlaylistsState {
	playlists: IPlaylist[];
	favs: string[];
}

export const initialState: PlaylistsState = {
	playlists: [],
	favs: []
};

const playlistsReducer = createReducer(
	initialState,
	on(PlaylistsActions.loadPlaylists, (state) => state),
	on(
		PlaylistsActions.loadPlaylistsSuccess,
		(state, { playlistsCollection }) => {
			return {
				...state,
				playlists: playlistsCollection
			};
		}
	),
	on(PlaylistsActions.loadPlaylistsError, (state) => state),
	on(PlaylistsActions.addPlaylistToFav, (state, { playlistToAdd }) => {
		return {
			...state,
			playlists: [...state.playlists],
			favs: [...state.favs, playlistToAdd]
		};
	})
);

export function reducer(state: PlaylistsState | undefined, action: Action) {
	return playlistsReducer(state, action);
}
