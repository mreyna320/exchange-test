import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, pluck, switchMap } from 'rxjs';

import * as PlaylistActions from './playlist.actions';
import { IPlaylistsResponse } from './playlist.model';

@Injectable({
	providedIn: 'root'
})
export class PlaylistsEffects {
	constructor(private actions$: Actions, private http: HttpClient) {}

	public loadPaymentMethods$ = createEffect(() =>
		this.actions$.pipe(
			ofType(PlaylistActions.loadPlaylists),
			switchMap(() => {
				return this.http
					.get<IPlaylistsResponse>(
						'https://portal.organicfruitapps.com/programming-guides/v2/us_en-us/featured-playlists.json'
					)
					.pipe(
						pluck('featuredPlaylists', 'content'),
						map((playlistsCollection) =>
							PlaylistActions.loadPlaylistsSuccess({
								playlistsCollection
							})
						),
						catchError(() => of(PlaylistActions.loadPlaylistsError))
					);
			})
		)
	);
}
