import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as PlaylistsActions from './playlist.actions';
import * as PlaylistSelectors from './playlist.selectors';
import { IPlaylist } from './playlist.model';

@Injectable({
	providedIn: 'root'
})
export class PlaylistsService {
	constructor(private store: Store<IPlaylist[]>) {}

	public loadPlaylists() {
		this.store.dispatch(PlaylistsActions.loadPlaylists());
	}

	public addPlaylistToFavs(playlist: IPlaylist) {
		this.store.dispatch(
			PlaylistsActions.addPlaylistToFav({
				playlistToAdd: playlist.id
			})
		);
	}

	public getPlaylistsCollection$(): Observable<IPlaylist[]> {
		return this.store.select(PlaylistSelectors.getPlaylistsCollection);
	}

	public getFavedPlaylists$(): Observable<IPlaylist[]> {
		return this.store.select(PlaylistSelectors.getFavedPlaylists);
	}

	public getFavedPlaylistsId$(): Observable<string[]> {
		return this.store.select(PlaylistSelectors.getFavedPlaylistsIds);
	}
}
