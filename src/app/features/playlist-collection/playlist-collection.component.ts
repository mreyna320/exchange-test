import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { IPlaylist } from './store/playlist.model';
import { PlaylistsService } from './store/playlists.service';

@Component({
	selector: 'app-playlist-collection',
	templateUrl: './playlist-collection.component.html',
	styleUrls: ['./playlist-collection.component.scss']
})
export class PlaylistCollectionComponent implements OnInit {
	public playlists$: Observable<IPlaylist[]>;

	constructor(private playlistsService: PlaylistsService) {}

	ngOnInit(): void {
		this.playlistsService.loadPlaylists();
		this.playlists$ = this.playlistsService.getPlaylistsCollection$();
	}

	addToFav(playlist: IPlaylist):void{
		this.playlistsService.addPlaylistToFavs(playlist);
	}
}
