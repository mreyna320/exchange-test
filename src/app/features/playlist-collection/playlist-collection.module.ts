import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../../shared/shared.module';
import { PlaylistCollectionComponent } from './playlist-collection.component';
import { PlaylistsEffects } from './store/playlists.effects';
import * as fromPlaylists from './store/playlists.reducer';

const routes: Routes = [{ path: '', component: PlaylistCollectionComponent }];

@NgModule({
	declarations: [PlaylistCollectionComponent],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		StoreModule.forFeature(
			fromPlaylists.playlistsFeatureKey,
			fromPlaylists.reducer
		),
		EffectsModule.forFeature([PlaylistsEffects])
	]
})
export class PlaylistCollectionModule {}
