import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';

@NgModule({
	declarations: [LayoutComponent],
	imports: [BrowserModule, RouterModule],
	exports: [LayoutComponent]
})
export class CoreModule {}
